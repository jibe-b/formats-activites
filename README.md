# Recencement de formats d'activité en groupe

Afin d'aligner les pratiques des individus d'un groupe poursuivant un même but, certains formats sont devenus des classiques.

Ces formats sont très présents dans les sessions du Rookie Club, en revanche il n'y a pas encore de ressource à ce sujet dans la bibliothèque.

Le répo http://gitlab.com/jibe-b/formats-activites contiendra :
- une liste de ressources existantes sur le sujet
- une série de fiches sur les différents formats

Il pourra bien évidemment être un point de départ pour une boîte d'où tirer des formats au hasard.